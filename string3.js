const string3=function(test_case)
{
    let num=test_case.toString().slice(0,2);
    let ans;
    switch(num)
    {
        case '01':
            ans= "January";
            break;
        case '02':
            ans= "February";
            break;
        case '03':
            ans= "March";
            break;
        case '04':
            ans= "April";
            break;
        case '05':
            ans= "May";
            break;
        case '06':
            ans= "June";
            break;
        case '07':
            ans= "July";
            break;
        case '08':
            ans= "August";
            break;
        case '09':
            ans= "September";
            break;
        case '10':
            ans= "October";
            break;
        case '11':
            ans= "November";
            break;
        case '12':
            ans= "December";
            break;
        default:
            ans="Wrong date";
    }
    return ans;
}


module.exports={
    string3
}