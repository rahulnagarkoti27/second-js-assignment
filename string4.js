let ans='';
function titleCase(str)
{
    str = str.toLowerCase().split(' ');
    for (var i = 0; i < str.length; i++)
    {
      str[i] = str[i].charAt(0).toUpperCase() + str[i].slice(1); 
    }
    return str.join(' ');
}
const string4=function(test_string)
{
    for (let value of Object.values(test_string)) {
        ans += value.toString();
        ans += " ";
    }
    ans=titleCase(ans);
    return ans;
}

module.exports={
    string4
}